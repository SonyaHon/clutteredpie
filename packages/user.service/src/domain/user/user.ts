import { DateTime } from 'luxon';

export type CreateUserProps = {
  id: string;
  username: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
  data: Record<string, unknown>;
};

export type UserServiceData = {
  refreshTokens: { token: string; createdAt: Date }[];
  isServiceAdmin: boolean;
};

export class User {
  private readonly id: string;
  private readonly username: string;
  private readonly password: string;
  private readonly createdAt: Date;
  private readonly updatedAt: Date;
  private readonly data: Record<string, unknown>;

  constructor({
    id,
    username,
    password,
    createdAt,
    updatedAt,
    data,
  }: CreateUserProps) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.data = data;
  }

  getId() {
    return this.id;
  }

  getUsername() {
    return this.username;
  }

  getPassword() {
    return this.password;
  }

  getCreatedAt() {
    return DateTime.fromJSDate(this.createdAt);
  }

  getUpdatedAt() {
    return DateTime.fromJSDate(this.updatedAt);
  }

  getDataForUserService(): UserServiceData {
    return this.getDataForService<UserServiceData>(
      'user-service',
    ) as UserServiceData;
  }

  getDataForService<T>(serviceKey: string): T | null {
    const data = this.data[serviceKey];
    if (!data) return null;
    return data as T;
  }
}
