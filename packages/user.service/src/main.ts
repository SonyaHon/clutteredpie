import { Logger } from '@nestjs/common';
import { bootstrap } from './bootstrap';

(async () => {
  try {
    await bootstrap();
  } catch (err) {
    const logger = new Logger('NestApplication');
    logger.error(err);
    process.exit(-1);
  }
})();
