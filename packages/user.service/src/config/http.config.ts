import { Config, EnvVar, String, Integer } from '@sonyahon/config';

@Config('HTTP')
export class HttpConfig {
  @EnvVar('PORT')
  @Integer()
  public port = 9090;

  @EnvVar('HOST')
  @String()
  public host = '0.0.0.0';
}
