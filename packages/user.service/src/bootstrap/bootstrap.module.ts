import { Module } from '@nestjs/common';
import { ConfigModule } from '@sonyahon/config';
import { PrismaModule } from '../adapters/prisma';
import { Configs } from '../config';

const ImportedModules = [
  ConfigModule.forRoot(Configs, { defineGlobal: true }),
  PrismaModule,
];

@Module({
  imports: [...ImportedModules],
})
export class BootstrapModule {}
