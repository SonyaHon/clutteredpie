import { INestApplication } from '@nestjs/common';

export type Maker = (application: INestApplication) => Promise<void> | void;
