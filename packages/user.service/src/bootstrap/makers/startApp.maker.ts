import { getConfigToken } from '@sonyahon/config';

import { HttpConfig } from '../../config/http.config';
import { Maker } from './base.maker';

export const startApp: Maker = async (app) => {
  const config: HttpConfig = app.get(getConfigToken(HttpConfig));
  await app.listen(config.port, config.host);
};
