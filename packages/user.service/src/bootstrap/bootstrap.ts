import { NestFactory } from '@nestjs/core';
import { BootstrapModule } from './bootstrap.module';
import { startApp } from './makers';

export const bootstrap = async () => {
  const application = await NestFactory.create(BootstrapModule, {});

  await startApp(application);
};
